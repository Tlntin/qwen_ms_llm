package qwen_ms_llm.mindspore

// 模块总文档：https://developer.huawei.com/consumer/cn/doc/harmonyos-references-V5/types_8h-V5


// 模型文件的类型。
public type MSModelType = Int64;
public struct MSModelTypeEnum {
  public static let MSMODELTYPE_MINDIR: Int64 = 0;
  public static let MSMODELTYPE_INVALID: Int64 = 0xFFFFFFFF;
}

// 设备类型信息，包含了目前支持的设备类型。
public type MSDeviceType = Int64;
public struct MSDeviceTypeEnum {
  public static let MSDEVICETYPE_CPU: Int64 = 0;
  public static let MSDEVICETYPE_GPU : Int64 = 1;
  public static let MSDEVICETYPE_KIRIN_NPU: Int64 = 2;
  public static let MSDEVICETYPE_NNRT:Int64 = 60;
  public static let MSDEVICETYPE_INVALID: Int64 = 100;
}

// NNRT管理的硬件设备类型。
public type MSNNRTDeviceType = Int64;
public struct MSNNRTDeviceTypeEnum {
  public static let MSNNRTDEVICE_OTHERS: Int64 = 0;
  public static let MSNNRTDEVICE_CPU: Int64 = 1;
  public static let MSNNRTDEVICE_GPU: Int64 = 2;
  public static let MSNNRTDEVICE_ACCELERATOR: Int64 = 3;
}

// NNRT硬件的工作性能模式。
public type MSPerformanceMode = Int64;
public struct MSPerformanceModeEnum {
  public static let MSPERFORMANCE_NONE: Int64 = 0;
  public static let MSPERFORMANCE_LOW: Int64 = 1;
  public static let MSPERFORMANCE_MEDIUM: Int64 = 2;
  public static let MSPERFORMANCE_HIGH: Int64 = 3;
  public static let MSPERFORMANCE_EXTREME: Int64 = 4;
}

// NNRT推理任务优先级。
public type MSPriority = Int64;
public struct MSPriorityEnum {
  public static let MSPRIORITY_NONE: Int64 = 0;
  public static let MSPRIORITY_LOW: Int64 = 1;
  public static let MSPRIORITY_MEDIUM: Int64 = 2;
  public static let MSPRIORITY_HIGH: Int64 = 3;
}

// 训练优化等级。
public type MSOptimizationLevel = Int64;
public struct MSOptimizationLevelEnum {
  public static let MSKO0: Int64 = 0;
  public static let MSKO2: Int64 = 2;
  public static let MSKO3: Int64 = 3;
  public static let MSKAUTO: Int64 = 4;
  public static let MSKOPTIMIZATIONTYPE = 0xFFFFFFFF;
}

// 量化类型信息
public type MSQuantizationType = Int64;
public struct MSQuantizationTypeEnum {
  public static let MSNO_QUANT: Int64 = 0;
  public static let MSWEIGHT_QUANT: Int64 = 1;
  public static let MSFULL_QUANT: Int64 = 2;
  public static let MSUNKNOWN_QUANT_TYPE: Int64 = 0xFFFFFFFF;
}
