## 项目概述
- 使用仓颉编程语言，接入mindspore-lite，完成对通义千问2（2.5系列应该也支持，一样的结构）的模型进行离线推理，目前测试了qwen2-0.5b-instruct.
- 适配仓颉版本：`0.53.13` ~ `0.55.3`
- 理论上支持Windows，不过bug很多，不太建议在Windows下面尝试。（Windows下Vscode终端运行不能输入中文，不过用自带终端可以。Windows有较大几率生成logits全为NaN，Linux则不会）

## 使用说明
1. 去Huggingface下载Qwen2系列的模型，例如`QWen2-0.5B-Instruct`，[下载地址](https://huggingface.co/Qwen/Qwen2-0.5B-Instruct)，[国内镜像下载地址](https://hf-mirror.com/Qwen/Qwen2-0.5B-Instruct)，放到download文件夹。
2. 获取mindspore-lite动态库
- 对于Linux系统，运行`download_ms_lib.sh`脚本，下载mindspore-lite相关so文件
- 对于Windows系统，如果你是Win10或者Win11系统，可以尝试使用powershell脚本直接运行`.\download_ms_lib.ps1`自动下载相关dll文件。
- 对于Windows系统，也可以手动下载压缩包，[下载链接](https://ms-release.obs.cn-north-4.myhuaweicloud.com/2.3.1/MindSpore/lite/release/windows/mindspore-lite-2.3.1-win-x64.zip)，解压该文件，然后将runtime/lib/下面所有dll结尾的动态库文件和runtime/lib/third_party/glog/下面所有dll结尾的动态库文件放到本项目根目录。
   ```bash
   runtime/lib/*.dll
   runtime/lib/third_party/glog/libmindspore_glog.dll
   ```

3. 放入.ms文件放到model文件夹，如果有.msw模型权重文件，则需要在项目根目录，和cjpm.toml文件同级。转pytorch原版模型为mindspore-lite专用的`.ms`文件，建议使用昇腾系列的NPU开发板完成该工作。参考这个项目，[链接](https://github.com/Tlntin/qwen-mindspore-lite-llm)
   - 如果你没有昇腾开发板或者想要快速体验一下，这里我提供一个转换好的qwen2-0.5b-instruct的ms文件。 链接：待上传。

4. 运行cjpm.run执行项目。