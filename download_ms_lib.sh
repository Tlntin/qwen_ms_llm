#!/bin/bash

VERSION_STR="2.3.1"
BASEPATH=$(cd "$(dirname $0)" || exit; pwd)
# get system arch
arch=$(uname -m)
if [ "$arch" = "aarch64" ]; then
    system_arch="aarch64"
elif [ "$arch" = "x86_64" ]; then
    system_arch="x64"
else
    system_arch="other"
fi
MINDSPORE_FILE_NAME="mindspore-lite-${VERSION_STR}-linux-${system_arch}"
MINDSPORE_FILE="${MINDSPORE_FILE_NAME}.tar.gz"
MINDSPORE_LITE_DOWNLOAD_URL="https://ms-release.obs.cn-north-4.myhuaweicloud.com/${VERSION_STR}/MindSpore/lite/release/linux/${arch}/${MINDSPORE_FILE}"

mkdir -p build
if [ ! -e ${BASEPATH}/build/${MINDSPORE_FILE} ]; then
  wget -c -O ${BASEPATH}/build/${MINDSPORE_FILE} --no-check-certificate ${MINDSPORE_LITE_DOWNLOAD_URL}
fi
tar xzvf ${BASEPATH}/build/${MINDSPORE_FILE} -C ${BASEPATH}/build/
cp -r ${BASEPATH}/build/${MINDSPORE_FILE_NAME}/runtime/lib/libmindspore-lite.so ${BASEPATH}
cp -r ${BASEPATH}/build/${MINDSPORE_FILE_NAME}/runtime/third_party/glog/libmindspore_glog.so* ${BASEPATH}/libmindspore_glog.so
cp -r ${BASEPATH}/build/${MINDSPORE_FILE_NAME}/runtime/third_party/glog/libmindspore_glog.so* ${BASEPATH}
