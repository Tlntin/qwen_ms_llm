$VERSION_STR = "2.3.1"
$BASEPATH = Split-Path -Parent $MyInvocation.MyCommand.Path

# Define the file name and URL
$MINDSPORE_FILE_NAME = "mindspore-lite-$VERSION_STR-win-x64"
$MINDSPORE_FILE = "$MINDSPORE_FILE_NAME.zip"
$MINDSPORE_LITE_DOWNLOAD_URL = "https://ms-release.obs.cn-north-4.myhuaweicloud.com/$VERSION_STR/MindSpore/lite/release/windows/$MINDSPORE_FILE"

# Create build directory if it doesn't exist
if (-not (Test-Path -Path "$BASEPATH\build")) {
    New-Item -ItemType Directory -Path "$BASEPATH\build"
}

# Download the file if it doesn't exist
if (-not (Test-Path -Path "$BASEPATH\build\$MINDSPORE_FILE")) {
    Invoke-WebRequest -Uri $MINDSPORE_LITE_DOWNLOAD_URL -OutFile "$BASEPATH\build\$MINDSPORE_FILE" -UseBasicParsing
}

# Extract the zip file
if (-not (Test-Path -Path "$BASEPATH\build\$MINDSPORE_FILE_NAME")) {
    Expand-Archive -Path "$BASEPATH\build\$MINDSPORE_FILE" -DestinationPath "$BASEPATH\build\"
}

# Copy the required files
Copy-Item -Path "$BASEPATH\build\$MINDSPORE_FILE_NAME\runtime\lib\*.dll" -Destination $BASEPATH
Copy-Item -Path "$BASEPATH\build\$MINDSPORE_FILE_NAME\runtime\third_party\glog\libmindspore_glog.dll" -Destination $BASEPATH
